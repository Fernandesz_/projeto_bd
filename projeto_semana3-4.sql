create database projeto;
use projeto;
SET SQL_SAFE_UPDATES = 0;
-- Estrutura Cliente 

create table cliente(
id INT,
nome VARCHAR(45),
telefone VARCHAR(45),
email VARCHAR(45),
primary key(id)
);


-- Estrutura vendedor

create table vendedor(
id INT,
nome VARCHAR(45),
primary key(id)
);

-- Estrutura pagamento

create table pagamento(
id INT NOT NULL,
forma_pagamento VARCHAR(45),
primary key(id)
);
-- Estrutra venda

create table venda(
id INT,
valor_total DECIMAL(10,2),
data datetime, 
cliente_id INT NOT NULL,
vendedor_id INT NOT NULL,
pagamento_id INT NOT NULL,
primary key(id),
foreign key(cliente_id) references cliente(id),
foreign key(vendedor_id) references vendedor(id),
foreign key(pagamento_id) references pagamento(id)
);

-- Estrutra  categoria

create table categoria(
id INT,
nome VARCHAR(45),
primary key(id)
);

-- Estrutra  fornecedores

create table fornecedores(
id INT,
nome VARCHAR(45),
cnpj VARCHAR(45),
primary key(id)
);

-- Estrutra  produto

create table produto(
id INT,
nome VARCHAR(45),
preço DECIMAL(10,2),
qtd_estoque VARCHAR(45),
categoria_id INT NOT NULL,
fornecedores_id INT NOT NULL,
primary key (id),
foreign key(categoria_id) references categoria(id),
foreign key(fornecedores_id) references fornecedores(id)
);

CREATE TABLE venda_has_produto (
  venda_id INT NOT NULL,
  produto_id INT NOT NULL,
  INDEX fk_venda_has_produto_produto1_idx (produto_id ASC) VISIBLE,
  INDEX fk_venda_has_produto_venda1_idx (venda_id ASC) VISIBLE,
  PRIMARY KEY (venda_id, produto_id),
  CONSTRAINT fk_venda_has_produto_venda1 FOREIGN KEY (venda_id) REFERENCES projeto.venda (id),
  CONSTRAINT fk_venda_has_produto_produto1 FOREIGN KEY (produto_id) REFERENCES projeto.produto (id)
  );


 -- Inserts
 
insert into cliente(id, nome, telefone, email) values 
('1', 'Allison Guilherme', '91111-1111', 'allison@gmail.com'), 
('2', 'Daniel Cazé', '92222-2222', 'daniel@outlook.com'),
('3', 'Daniel Vitor', '93333-3333', 'danivel_victor@hotmail.com'),
('4', 'Marcos Vinicius', '94444-4444', 'marcosv@gmail.com'),
('5', 'Emanuel Lira', '95555-5555', 'Ravinho@gmail.com'),
('6', 'Arthur Lira', '96666-6666', 'Arth009@gmail.com'),
('7', 'Fabio Felipe', '97777-7777', 'Felps@gmail.com'),
('8', 'Álvaro Jorge', '98888-8888', 'Alvaro@gmail.com'),
('9', 'Walter Travassos', '99999-9999', 'WalterTravasso@gmail.com'),
('10', 'Violet', 91010-1010, 'violet@gmail.com');

insert into vendedor(id, nome) values 
('1', 'Ihago'),
('2', 'Vinicios'),
('3', 'Rony'),
('4', 'Lucas'),
('5', 'Escalvino'),
('6', 'Rick'),
('7', 'Roberto'),
('8', 'Walace'),
('9', 'Diego'),
('10', 'Bernado');

insert into pagamento(id, forma_pagamento) values 
('1', 'Dinheiro'),
('2', 'Crédito'),
('3', 'Débito'),
('4', 'Aproximação'),
('5', 'Boleto'),
('6', 'Transferência'),
('7', 'Pix'),
('8', 'Cheque'),
('9', 'paypal'),
('10', 'picpay');

insert into venda (id, valor_total, data, cliente_id, vendedor_id, pagamento_id) values
('1', '11.00','2021-01-01 10:00:00', '1', '1', '1'),
('2', '2941.76','2021-02-02 11:00:00', '2','2','2'),
('3', '474.80','2021-03-03 12:00:00', '3','3','3'),
('4', '33.00','2021-04-04 13:00:00', '4','4','4'),
('5', '537.40','2021-05-05 14:00:00', '5','5','5'),
('6', '65.80','2021-06-06 15:00:00', '6','6','6'),
('7', '70.00','2021-07-07 16:00:00', '7','7','7'),
('8', '7650.00','2021-08-08 17:00:00', '8','8','8'),
('9', '92.60','2021-09-09 18:00:00', '9','9','9'),
('10', '8791.76','2021-10-03 16:00:00', '10','10','10');

insert into categoria (id, nome) values
('1', 'Celulares'),
('2', 'Periféricos'),
('3', 'Roupas'),
('4', 'Higiene pessoal'),
('5', 'Cozinha'),
('6', 'Esportes'),
('7', 'Papelaria'),
('8', 'Doces'),
('9', 'Eletrônicos'),
('10', 'Livros');

insert into fornecedores (id, nome, cnpj) values
('1', 'Xioami', '111.111.111/0001-11'),
('2', 'Logitech','222.222.222/0001-22'),
('3','Lacoste','333.333.333/0001-33'),
('4','Unilever','444.444.444/0001-44'),
('5','Polishop','555.555.555/0001-55'),
('6', 'Nike', '666.666.666/0001-66'),
('7','Tilibra','777.777.777/0001-77'),
('8','Nestlé','888.888.888/0001-88'),
('9','Sony','999.999.999/0001-99'),
('10','Amazon','123.456.789/0001-10');

insert into produto (id, nome, preço, qtd_estoque, categoria_id, fornecedores_id) values
('1','Mi 11 Ultra','1800.00','10','1','1'),
('2','Headset Gamer G935','1141.76','15','2','2'),
('3','Camisa Polo Branca','350.00','20','3','3'),
('4','Desodorante','16.50','500','4','4'),
('5','Frigideira FlavorStone','237.40','12','5','5'),
('6','Chuteira Mercurial','300.00','10','6','6'),
('7','Caderno 20 matérias','46.30','25','7','7'),
('8','Barra de Chocolate','5.50','20','8','8'),
('9','Playstation 5','7650.00','8','9','9'),
('10','Uma breve história do tempo','32.90','20','10','10');

-- script 1
select categoria_id"categoria do produto", qtd_estoque"quantidade em estoque" from produto;

-- script 2
select cliente.nome, cliente.email, venda.valor_total "valor da venda"
from venda INNER JOIN cliente on cliente.id = venda.id where venda.data between date('2021-07-01') and date('2021-07-31');

-- script 3
-- o script foi pedido com a letra J, mas como não tem ninguém com a letra J eu mudei a letra por D, espero que não tenha problema.
select telefone, email, nome from cliente where nome LIKE 'd%' order by email desc;

-- script 4
update produto set preço = (preço * 1.12) where qtd_estoque < 15;

-- script 5
select vendedor.nome, venda.data, pagamento.forma_pagamento 
from venda INNER JOIN vendedor INNER JOIN pagamento ON vendedor.id = venda.id and pagamento.id = venda.id;

-- script 6
create view v_produtos
as
select produto.nome"Produto", categoria.nome"Categoria", fornecedores.nome"Fornecedor" 
from produto INNER JOIN fornecedores INNER JOIN categoria on fornecedores.id = produto.id and categoria.id = produto.id where preço < 100;

drop view v_produtos;
select * from v_produtos;
-- script 7
select data from venda where (valor_total between 100 and 500) and (venda.data between '2021-01-01' and '2021-12-31');

-- script 8

-- script 9
select produto.id, produto.nome, produto.preço, produto.qtd_estoque "Estoque", categoria.nome "Categoria", fornecedores.nome "Fornecedores"
from produto INNER JOIN categoria INNER JOIN fornecedores on categoria.id = produto.id and fornecedores.id = produto.id;

-- script 10
select preço from produto;
select fornecedores.nome, preço from produto INNER JOIN fornecedores on fornecedores.id = produto.id where preço = (select max(preço) from produto);

select* from cliente;
select* from vendedor;
select* from pagamento;
select* from venda;
select* from categoria;
select* from fornecedores;
select* from produto;




-- drop database projeto;